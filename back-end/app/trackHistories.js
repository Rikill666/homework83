const express = require('express');
const TrackHistory = require('../models/TrackHistory');
const User = require('../models/User');
const Track = require('../models/Track');
const router = express.Router();

router.post('/', async (req, res) => {

    const authorizationHeader = req.get('Authorization');

    if(!authorizationHeader ){
        return res.status(401).send({error: "Access is denied!!!"});
    }

    const [type, token] = authorizationHeader.split(' ');

    if(type!=='Token' || !token){
        return res.status(401).send({error: "Access is denied!!!"});
    }

    const user = await User.findOne({token: token});
    if(!user){
        return res.status(401).send({error: "Access is denied!!!"});
    }
    const trackId = req.query.track;
    const track = await Track.findOne({_id: trackId});
    if(!track){
        return res.status(400).send({error: "Wrong id track"});
    }
    try {
        const trackHistory = new TrackHistory({user:user._id, track: trackId, dateTime: new Date()});
        await trackHistory.save();
        return res.send(trackHistory);
    } catch (error) {
        return res.status(400).send(error);
    }
});

module.exports = router;