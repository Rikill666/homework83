const express = require('express');
const nanoid = require('nanoid');
const multer = require('multer');
const path = require('path');
const Album = require('../models/Album');
const config = require('../config');


const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath + '/albumsImg');
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});
const router = express.Router();
const upload = multer({storage});

router.get("/", async (req, res) => {
    let albums;
    if(req.query.artist){
        albums = await Album.find({"artist":req.query.artist});
    }
    else{
        albums = await Album.find();
    }
    res.send(albums);
});

router.get("/:id", async (req, res) => {
    const id = req.params.id;
    if(id){
        const albums = await Album.find({"_id":id}).populate('artist');
        const album = albums[0];
        res.send(album);
    }
});

router.post('/',  upload.single('coverImage'), async (req, res) => {
    const newAlbum = req.body;
    if (req.file) {
        newAlbum.coverImage = req.file.filename;
    }
    try {
        const album = new Album(newAlbum);
        await album.save();
        res.send(album);
    } catch (error) {
        return res.status(400).send(error);
    }
});

module.exports = router;