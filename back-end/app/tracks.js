const express = require('express');
const Track = require('../models/Track');
const Album = require('../models/Album');

const router = express.Router();

router.get("/", async (req, res) => {
    let tracks;
    if(req.query.artist){
        const albums = await Album.find({"artist":req.query.artist});
        tracks = (await Promise.all(albums.map(a => {
            return Track.find({"album": a._id});
        }))).flat();
    }
    else if(req.query.album){
        tracks = await Track.find({"album":req.query.album});
    }
    else{
        tracks = await Track.find();
    }
    res.send(tracks);
});

router.post('/', async (req, res) => {
    try {
        const track = new Track(req.body);
        await track.save();
        res.send(track);
    } catch (error) {
        return res.status(400).send(error);
    }
});

module.exports = router;