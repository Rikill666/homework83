const express = require('express');
const nanoid = require('nanoid');
const multer = require('multer');
const path = require('path');
const Artist = require('../models/Artist');
const config = require('../config');


const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath + '/artistsImg');
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});
const router = express.Router();
const upload = multer({storage});

router.get("/", async (req, res) => {
    const artists = await Artist.find();
    res.send(artists);
});

router.post('/',  upload.single('image'), async (req, res) => {
    const newArtist = req.body;
    if (req.file) {
        newArtist.image = req.file.filename;
    }
    try {
        const artist = new Artist(newArtist);
        await artist.save();
        res.send(artist);
    } catch (error) {
        return res.status(400).send(error);
    }
});
module.exports = router;