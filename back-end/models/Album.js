const mongoose = require("mongoose");

const Scheme = mongoose.Schema;

const albumScheme = new Scheme({
    title:{
        type: String,
        required: true
    },
    releaseDate:{
        type: Date,
        required: true
    },
    coverImage:{
        type: String
    },
    artist:{
        type: Scheme.Types.ObjectId,
        ref: 'Artist',
        required: true
    }
});

const Album = mongoose.model("Album", albumScheme);
module.exports = Album;