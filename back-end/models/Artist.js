const mongoose = require("mongoose");

const Scheme = mongoose.Schema;

const artistScheme = new Scheme({
    name:{
        type: String,
        required:true
    },
    image:{
        type: String
    },
    description:{
        type: String
    }
});

const Artist = mongoose.model("Artist", artistScheme);
module.exports = Artist;